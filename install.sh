#!/bin/bash
if ! [ -t 0 ]; then
  echo "exec from pipe"
  DOWNLOAD_APP=y
  APP_DIR="$HOME/.local/share/madumlao-profile"
else
  DOWNLOAD_APP=
  APP_DIR="$(dirname "$0")"
fi

if [ "$DOWNLOAD_APP" ]; then
  echo "Downloading master copy"
  PARENT_DIR="$(dirname "$APP_DIR")"
  mkdir -pv "$PARENT_DIR"
  if [ -x git ]; then
    cd "$PARENT_DIR"
    git clone https://gitlab.com/madumlao/madumlao-profile
  else
    curl -O https://gitlab.com/madumlao/madumlao-profile/-/archive/master/madumlao-profile-master.tar.gz
    tar -zxf madumlao-profile-master.tar.gz -C "$PARENT_DIR"
  fi
fi

echo "Set pwd to $APP_DIR"
cd "$APP_DIR"
APP_DIR="$(pwd)"

if [ "$(uname)" == "Linux" ]; then
  # TODO: add support for different distributions
  ./install-ubuntu.sh
elif [ "$(uname)" == "Darwin" ]; then
  echo TODO: install-macos.sh
else
  echo Unsupported environment, need to manual install
fi
